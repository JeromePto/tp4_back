"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var metrics_1 = require("./metrics");
var leveldb_1 = require("./leveldb");
// const dbPath: string = "./db/test";
var dbPath = "./db/test";
var dbMet;
describe("Metrics", function () {
    before(function () {
        leveldb_1.LevelDB.clear(dbPath);
        dbMet = new metrics_1.MetricsHandler(dbPath);
        console.log("Database Opened");
    });
    after(function () {
        dbMet.close();
        console.log("Database closed");
    });
    describe("#get", function () {
        it("should get empty array on non existing group", function (done) {
            dbMet.get("0", function (err, result) {
                chai_1.expect(err).to.be.null;
                chai_1.expect(result).to.not.be.undefined;
                chai_1.expect(result).to.be.empty;
                done();
            });
        });
    });
    describe("#save", function () {
        it("should add data to the db", function (done) {
            dbMet.save("6", [new metrics_1.Metric("1234", 42)], function (err) {
                chai_1.expect(err).to.be.null;
                dbMet.get("6", function (err, result) {
                    chai_1.expect(err).to.be.null;
                    chai_1.expect(result).to.not.be.undefined;
                    if (result) {
                        chai_1.expect(result[0].timestamp).to.be.equal("1234");
                        chai_1.expect(result[0].value).to.be.equal(42);
                    }
                    done();
                });
            });
        });
        it("should updtae data ", function (done) {
            dbMet.save("6", [new metrics_1.Metric("1234", 32)], function (err) {
                chai_1.expect(err).to.be.null;
                dbMet.get("6", function (err, result) {
                    chai_1.expect(err).to.be.null;
                    chai_1.expect(result).to.not.be.undefined;
                    if (result) {
                        chai_1.expect(result[0].timestamp).to.be.equal("1234");
                        chai_1.expect(result[0].value).to.be.equal(32);
                    }
                    done();
                });
            });
        });
    });
    describe("#delete", function () {
        it("should delete the previous data", function (done) {
            dbMet.delete("6", function (err) {
                chai_1.expect(err).to.be.null;
                dbMet.get("6", function (err) {
                    chai_1.expect(err).to.be.null;
                    done();
                });
            });
        });
        it("should try to delete and don't throw error", function (done) {
            dbMet.delete("6", function (err) {
                chai_1.expect(err).to.be.null;
                dbMet.get("6", function (err) {
                    chai_1.expect(err).to.be.null;
                    done();
                });
            });
        });
    });
});
