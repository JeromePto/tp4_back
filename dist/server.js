"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var path = require("path");
var bodyparser = require("body-parser");
var morgan_1 = __importDefault(require("morgan"));
var session = require("express-session");
var levelSession = require("level-session-store");
var leveldb_1 = require("./leveldb");
var metrics_1 = require("./metrics");
var user_1 = require("./user");
var app = express();
var LevelStore = levelSession(session);
var port = process.env.PORT || "8080";
app.use(express.static(path.join(__dirname, "/../public")));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));
app.set("views", __dirname + "/../views");
app.set("view engine", "ejs");
leveldb_1.LevelDB.clear("./db/users");
var dbMet = new metrics_1.MetricsHandler("./db/metrics");
var dbUser = new user_1.UserHandler("./db/users");
app.use(morgan_1.default("dev"));
app.use(session({
    secret: "mon secret",
    store: new LevelStore("./db/sessions"),
    resave: true,
    saveUninitialized: true,
}));
app.get("/dev/req", function (req, res) {
    res.send(req.session);
});
dbUser.save(new user_1.User("aaa", "aaa@aaa.fr", "aaa"), function (err) {
    if (err) {
        throw err;
    }
});
var authRouter = express.Router();
authRouter.get("/login", function (req, res) {
    res.render("login");
});
authRouter.get("/signup", function (req, res) {
    res.render("signup");
});
authRouter.get("/logout", function (req, res) {
    delete req.session.loggedIn;
    delete req.session.user;
    res.redirect("/login");
});
app.post("/login", function (req, res, next) {
    dbUser.get(req.body.username, function (err, result) {
        // console.log("login error: " + err);
        if (err)
            return next(err);
        if (result === undefined ||
            !result.validatePassword(req.body.password)) {
            res.redirect("/login");
        }
        else {
            req.session.loggedIn = true;
            req.session.user = result;
            res.redirect("/");
        }
    });
});
app.use(authRouter);
var userRouter = express.Router();
userRouter.post("/", function (req, res, next) {
    dbUser.get(req.body.username, function (err, result) {
        if (!err || result !== undefined) {
            res.status(409).send("user already exists");
        }
        else {
            dbUser.save(new user_1.User(req.body.username, req.body.email, req.body.password), function (err) {
                if (err)
                    next(err);
                else
                    res.status(201).send("user persisted");
            });
        }
    });
});
userRouter.get("/:username", function (req, res, next) {
    dbUser.get(req.params.username, function (err, result) {
        if (err || result === undefined) {
            res.status(404).send("user not found");
        }
        else
            res.status(200).json(result);
    });
});
userRouter.delete("/:username", function (req, res, next) {
    dbUser.get(req.params.username, function (err, result) {
        if (err || result === undefined) {
            res.status(404).send("user not found");
        }
        else {
            dbUser.delete(req.params.username, function (err) {
                if (err) {
                    return next(err);
                }
                else {
                    if (req.session.loggedIn === true &&
                        req.params.username === req.session.user.username) {
                        delete req.session.loggedIn;
                        delete req.session.user;
                    }
                    res.status(200).send("user deleted");
                }
            });
        }
    });
});
app.use("/user", userRouter);
var authCheck = function (req, res, next) {
    if (req.session.loggedIn) {
        next();
    }
    else
        res.redirect("/login");
};
app.get("/", authCheck, function (req, res) {
    res.render("index", { name: req.session.user.username });
});
// app.get("/", (req: any, res: any) => {
//     res.write("Hello world");
//     res.end();
// });
app.get("/hello/:name", function (req, res) {
    res.render("hello.ejs", { name: req.params.name });
});
app.get("/metrics/:id", function (req, res) {
    dbMet.get(req.params.id, function (err, result) {
        if (err)
            throw err;
        res.json(result);
    });
});
app.get("/metrics", function (req, res) {
    dbMet.getAll(function (err, result) {
        if (err)
            throw err;
        res.json(result);
    });
});
app.post("/metrics/:id", function (req, res) {
    dbMet.save(req.params.id, req.body, function (err) {
        if (err)
            throw err;
        res.status(200).send("OK");
    });
});
app.delete("/metrics/:id", function (req, res) {
    dbMet.delete(req.params.id, function (err) {
        if (err)
            throw err;
        res.status(200).send("OK");
    });
});
app.listen(port, function (err) {
    if (err)
        throw err;
    console.log("Server is running on http://localhost:" + port);
});
