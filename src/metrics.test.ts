import { expect } from "chai";
import { Metric, MetricsHandler } from "./metrics";
import { LevelDB } from "./leveldb";

// const dbPath: string = "./db/test";
const dbPath: string = "./db/test";
var dbMet: MetricsHandler;

describe("Metrics", function() {
    before(function() {
        LevelDB.clear(dbPath);
        dbMet = new MetricsHandler(dbPath);
        console.log("Database Opened");
    });

    after(function() {
        dbMet.close();
        console.log("Database closed");
    });

    describe("#get", function() {
        it("should get empty array on non existing group", function(done) {
            dbMet.get("0", function(err: Error | null, result?: Metric[]) {
                expect(err).to.be.null;
                expect(result).to.not.be.undefined;
                expect(result).to.be.empty;
                done();
            });
        });
    });

    describe("#save", function() {
        it("should add data to the db", function(done) {
            dbMet.save("6", [new Metric("1234", 42)], (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null;
                    expect(result).to.not.be.undefined;
                    if(result) {
                        expect(result[0].timestamp).to.be.equal("1234");
                        expect(result[0].value).to.be.equal(42);
                    }
                    done();
                });
            });
        });
        it("should updtae data ", function(done) {
            dbMet.save("6", [new Metric("1234", 32)], (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null, result?: Metric[]) {
                    expect(err).to.be.null;
                    expect(result).to.not.be.undefined;
                    if (result) {
                        expect(result[0].timestamp).to.be.equal("1234");
                        expect(result[0].value).to.be.equal(32);
                    }
                    done();
                });
            });
        });
    });

    describe("#delete", () => {
        it("should delete the previous data", done => {
            dbMet.delete("6", (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null) {
                    expect(err).to.be.null;
                    done();
                });
            });
        });

        it("should try to delete and don't throw error", done => {
            dbMet.delete("6", (err: Error | null) => {
                expect(err).to.be.null;

                dbMet.get("6", function(err: Error | null) {
                    expect(err).to.be.null;
                    done();
                });
            });
        });
    });
});
