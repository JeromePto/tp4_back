import express = require("express");
import path = require("path");
import bodyparser = require("body-parser");
import morgan from "morgan";
import session = require("express-session");
import levelSession = require("level-session-store");
import { LevelDB } from "./leveldb";

import { MetricsHandler } from "./metrics";
import { UserHandler, User } from "./user";

const app = express();
const LevelStore = levelSession(session);
const port: string = process.env.PORT || "8080";

app.use(express.static(path.join(__dirname, "/../public")));
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ extended: false }));

app.set("views", __dirname + "/../views");
app.set("view engine", "ejs");

LevelDB.clear("./db/users");

const dbMet: MetricsHandler = new MetricsHandler("./db/metrics");
const dbUser: UserHandler = new UserHandler("./db/users");

app.use(morgan("dev"));

app.use(
    session({
        secret: "mon secret",
        store: new LevelStore("./db/sessions"),
        resave: true,
        saveUninitialized: true,
    })
);

app.get("/dev/req", (req: any, res: any) => {
    res.send(req.session);
});

dbUser.save(new User("aaa", "aaa@aaa.fr", "aaa"), err => {
    if (err) {
        throw err;
    }
});

const authRouter = express.Router();

authRouter.get("/login", (req: any, res: any) => {
    res.render("login");
});

authRouter.get("/signup", (req: any, res: any) => {
    res.render("signup");
});

authRouter.get("/logout", (req: any, res: any) => {
    delete req.session.loggedIn;
    delete req.session.user;
    res.redirect("/login");
});

app.post("/login", (req: any, res: any, next: any) => {
    dbUser.get(req.body.username, (err: Error | null, result?: User) => {
        // console.log("login error: " + err);
        if (err) return next(err);
        if (
            result === undefined ||
            !result.validatePassword(req.body.password)
        ) {
            res.redirect("/login");
        } else {
            req.session.loggedIn = true;
            req.session.user = result;
            res.redirect("/");
        }
    });
});

app.use(authRouter);

const userRouter = express.Router();

userRouter.post("/", (req: any, res: any, next: any) => {
    dbUser.get(req.body.username, function(err: Error | null, result?: User) {
        if (!err || result !== undefined) {
            res.status(409).send("user already exists");
        } else {
            dbUser.save(
                new User(req.body.username, req.body.email, req.body.password),
                function(err: Error | null) {
                    if (err) next(err);
                    else res.status(201).send("user persisted");
                }
            );
        }
    });
});

userRouter.get("/:username", (req: any, res: any, next: any) => {
    dbUser.get(req.params.username, function(err: Error | null, result?: User) {
        if (err || result === undefined) {
            res.status(404).send("user not found");
        } else res.status(200).json(result);
    });
});

userRouter.delete("/:username", (req: any, res: any, next: any) => {
    dbUser.get(req.params.username, (err: Error | null, result?: User) => {
        if (err || result === undefined) {
            res.status(404).send("user not found");
        } else {
            dbUser.delete(req.params.username, (err: Error | null) => {
                if (err) {
                    return next(err);
                } else {
                    if (
                        req.session.loggedIn === true &&
                        req.params.username === req.session.user.username
                    ) {
                        delete req.session.loggedIn;
                        delete req.session.user;
                    }
                    res.status(200).send("user deleted");
                }
            });
            
        }
    });
});

app.use("/user", userRouter);

const authCheck = function(req: any, res: any, next: any) {
    if (req.session.loggedIn) {
        next();
    } else res.redirect("/login");
};

app.get("/", authCheck, (req: any, res: any) => {
    res.render("index", { name: req.session.user.username });
});

// app.get("/", (req: any, res: any) => {
//     res.write("Hello world");
//     res.end();
// });

app.get("/hello/:name", (req: any, res: any) => {
    res.render("hello.ejs", { name: req.params.name });
});

app.get("/metrics/:id", (req: any, res: any) => {
    dbMet.get(req.params.id, (err: Error | null, result?: any) => {
        if (err) throw err;
        res.json(result);
    });
});
app.get("/metrics", (req: any, res: any) => {
    dbMet.getAll((err: Error | null, result?: any) => {
        if (err) throw err;
        res.json(result);
    });
});

app.post("/metrics/:id", (req: any, res: any) => {
    dbMet.save(req.params.id, req.body, (err: Error | null) => {
        if (err) throw err;
        res.status(200).send("OK");
    });
});

app.delete("/metrics/:id", (req: any, res: any) => {
    dbMet.delete(req.params.id, (err: Error | null) => {
        if (err) throw err;
        res.status(200).send("OK");
    });
});

app.listen(port, (err: Error) => {
    if (err) throw err;
    console.log(`Server is running on http://localhost:${port}`);
});
